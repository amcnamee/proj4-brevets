# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

## ACP controle times

Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders).

It implements the calculator at (https://rusa.org/octime_acp.html) with AJAX and Flask.

The first control closes an hour after the ride begins.

This calculator uses French rules: when a control is at 60 or less km from the start, it closes at 20km/hr + 1hr.

Otherwise, the calculator uses this table (from rusa.org)

| Control location (km) | Minimum Speed (km/hr) | Maximum Speed (km/hr) |
|-----------------------|-----------------------|-----------------------|
| 0 - 200               | 15                    | 34                    |
| 200 - 400             | 15                    | 32                    |
| 400 - 600             | 15                    | 30                    |
| 600 - 1000            | 11.428                | 28                    |

The last control can be at or less than 20% greater than the length of the brevet and closes at:

* 13:30 for 200 KM

* 20:00 for 300 KM

* 27:00 for 400 KM

* 40:00 for 600 KM

* 75:00 for 1000 KM  

## AJAX and Flask reimplementation

The RUSA controle time calculator is a Perl script that takes an HTML form and emits a text page in the above link.

* Each time a distance is filled in, the corresponding open and close times are filled in.   

## Author

Audra McNamee

amcnamee@uoregon.edu
