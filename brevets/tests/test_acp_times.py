"""
Nose tests for jumble.py

We cannot test for randomness here (no effective oracle),
but we can test that the elements in the returned string
are correct.
"""

from acp_times import open_time, close_time

import nose    # Testing framework
import logging
import arrow
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

"""
def test_bad_starts():
    # the test raises an exception-> it will be an error message? (bc negative)
    assert same(jumbled(["abbcd"], 1), "abbcd")

def test_bad_ends():
    # past 20% after
    assert same(jumbled(["abbc", "abcc"], 2), "abbcc")
"""

start_time = arrow.get('2020-11-01 06:30:00', 'YYYY-MM-DD HH:mm:ss').isoformat()

start_time2 = arrow.get('2020-11-01 01:00:00', 'YYYY-MM-DD HH:mm:ss').isoformat()


def test_early_checkpoint_open():
    assert open_time(0, 200, start_time2) == arrow.get('2020-11-01 01:00:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert open_time(50, 200, start_time2) == arrow.get('2020-11-01 02:28:00', 'YYYY-MM-DD HH:mm:ss').isoformat()

    assert open_time(0, 200, start_time) == arrow.get('2020-11-01 06:30:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert open_time(50, 200, start_time) == arrow.get('2020-11-01 07:58:00', 'YYYY-MM-DD HH:mm:ss').isoformat()

def test_mid_checkpoint_open():
    # general tests for checkpoints in the middle of brevets
    assert open_time(100, 200, start_time2) == arrow.get('2020-11-01 03:56:00', 'YYYY-MM-DD HH:mm:ss').isoformat()

    assert open_time(100, 200, start_time) == arrow.get('2020-11-01 09:26:00', 'YYYY-MM-DD HH:mm:ss').isoformat()

def test_end_checkpoint_open():
    # tests 200/400/600/1000km brevets
    assert open_time(200, 200, start_time2) == arrow.get('2020-11-01 06:53:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert open_time(205, 200, start_time2) == arrow.get('2020-11-01 06:53:00', 'YYYY-MM-DD HH:mm:ss').isoformat()

    assert open_time(200, 200, start_time) == arrow.get('2020-11-01 12:23:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert open_time(205, 200, start_time) == arrow.get('2020-11-01 12:23:00', 'YYYY-MM-DD HH:mm:ss').isoformat()

def test_early_checkpoint_close():
    # tests french rules apply
    assert close_time(0, 200, start_time2) == arrow.get('2020-11-01 02:00:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert close_time(50, 200, start_time2) == arrow.get('2020-11-01 04:30:00', 'YYYY-MM-DD HH:mm:ss').isoformat()

    assert close_time(0, 200, start_time) == arrow.get('2020-11-01 07:30:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert close_time(50, 200, start_time) == arrow.get('2020-11-01 10:00:00', 'YYYY-MM-DD HH:mm:ss').isoformat()

def test_mid_checkpoint_close():
    # general tests for checkpoints in the middle of brevets
    assert close_time(100, 200, start_time2) == arrow.get('2020-11-01 07:40:00', 'YYYY-MM-DD HH:mm:ss').isoformat()

    assert close_time(100, 200, start_time) == arrow.get('2020-11-01 13:10:00', 'YYYY-MM-DD HH:mm:ss').isoformat()

    assert close_time(100, 1000, start_time2) == arrow.get('2020-11-01 07:40:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert close_time(100, 1000, start_time) == arrow.get('2020-11-01 13:10:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert close_time(100, 300, start_time2) == arrow.get('2020-11-01 07:40:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert close_time(100, 300, start_time) == arrow.get('2020-11-01 13:10:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert close_time(100, 400, start_time2) == arrow.get('2020-11-01 07:40:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert close_time(100, 400, start_time) == arrow.get('2020-11-01 13:10:00', 'YYYY-MM-DD HH:mm:ss').isoformat()

def test_past_end_close():
    # tests that checkpoints 20% after ends give right response
    # close_time(control_dist_km, brevet_dist_km, brevet_start_time)
    assert close_time(200, 200, start_time2) == arrow.get('2020-11-01 14:30:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert close_time(205, 200, start_time2) == arrow.get('2020-11-01 14:30:00', 'YYYY-MM-DD HH:mm:ss').isoformat()

    assert close_time(200, 200, start_time) == arrow.get('2020-11-01 20:00:00', 'YYYY-MM-DD HH:mm:ss').isoformat()
    assert close_time(205, 200, start_time) == arrow.get('2020-11-01 20:00:00', 'YYYY-MM-DD HH:mm:ss').isoformat()


# test days switching etc
