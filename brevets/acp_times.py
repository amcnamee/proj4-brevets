"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    # take care of case when the thing is past the end
    if brevet_dist_km <= control_dist_km:
        control_dist_km = brevet_dist_km


    open_list = [(600, 28), (400, 30), (200, 32), (0, 34)]
    time_opens = 0

    for i in open_list:
        len, speed = i
        if control_dist_km > len: # part of the control is at given speed
            diff = control_dist_km - len
            time_opens += (diff / speed)
            control_dist_km -= diff

    open = shift(brevet_start_time, time_opens)
    return open.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """

    close_list = [(600, 11.428), (400, 13), (200, 15), (0, 15)]
    time_close = 0

    if brevet_dist_km <= control_dist_km:
        # the check that these values are legal occurs outside of the function
        # last gate closes: 13:30 for 200 KM, 20:00 for 300 KM, 27:00 for 400 KM, 40:00 for 600 KM, and 75:00 for 1000 KM
        last_gate = {200:13.5, 300:20, 400:27, 600:40, 1000:75}
        time_close = last_gate[brevet_dist_km]

    elif control_dist_km <= 60:
        # french rules apply
        time_close = control_dist_km / 20 # 20 km/hr is the speed
        time_close += 1 # plus 1 hour
        #return shift(brevet_start_time, time_close).isoformat()

    else:
        for i in close_list:
            len, speed = i
            print(f"len is {len}, speed is {speed}")
            if control_dist_km > len:
                diff = control_dist_km - len
                print(f"diff is {diff}")
                time_close += diff / speed
                control_dist_km -= diff

    close = shift(brevet_start_time, time_close)

    return close.isoformat()

def shift(start_time, time_passed):
    """ Takes an arrow, shifts it by hour and minute indicated by time """

    hr = time_passed // 1 # integer portion of time open
    min = (time_passed % 1) * 60 # how many minutes (rounded to closest half)

    # sec = (min % 1) * 60
    # if sec > 30:
    #     min += 1
    min = round(min)

    #print(sec, "sec", min, "min", hr, "hr", time_passed)

    ret_time = arrow.get(start_time)
    ret_time = ret_time.shift(hours=+hr, minutes=+min)

    print(ret_time)
    return ret_time
