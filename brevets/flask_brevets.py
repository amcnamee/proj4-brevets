"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects four URL-encoded arguments: the number of kilometers, the length of the brevet, and the start time and date.
    """
    # change to also take a time
    # Using Jquery ID selectors and then passing the date and time to the server as separate arguments
    #date_time_string = date + ' ' + time + ':00'
    #date_time_arrow = arrow.get(date_time_string, 'YYYY-MM-DD HH:mm:ss')

    """The easiest way I found to get those was to use the JQuery ID selectors to get the .val(); fields of the date and time boxes, then use moment.js's formatting strings to turn the date and time into an ISO string that you can pass to the server.
    As far as actually passing values from the HTML file to the flask server, you can do that by adding new elements to the input dictionary in the HTML file (the one containing {km: km}), which you should then be able to parse in the server file! """

    app.logger.debug("Got a JSON request")

    km = request.args.get('km', 999, type=float)
    brevet_len = request.args.get('brevet_len', 999, type=int)
    start_date = request.args.get('start_date', type=str)
    start_time = request.args.get('start_time', type=str)
    # turn the date and time into an arrow
    start_arrow = arrow.get(start_date + ' ' + start_time + ':00', 'YYYY-MM-DD HH:mm:ss').isoformat()

    app.logger.debug(start_arrow)

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    result = {"open": None, "close": None, "error":0, "message": None}

    if km < 0:
        result["error"] = 1
        result["message"] = "Error: negative"

    elif (km - brevet_len)/brevet_len > .2:
        result["error"] = 1
        result["message"] = "Error: too far"

    else:
        open_time = acp_times.open_time(km, brevet_len, start_arrow)
        close_time = acp_times.close_time(km, brevet_len, start_arrow)
        result = {"open": open_time, "close": close_time}



    # FIXME: test if km is valid-- not too big, not too small
    # open_time = acp_times.open_time(km, brevet_len, start_arrow)
    # close_time = acp_times.close_time(km, brevet_len, start_arrow)
    # result = {"open": open_time, "close": close_time, "error": }
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
